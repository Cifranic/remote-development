#!/bin/bash
# Run this script locally.  It will sync entire directory, and updates, and send to remote host.
# Syncs every second, but only if file has changes locally.
# ensure rsync is installed on localhost

# location of local folder to share:
# NOTE: format must match example
LOCAL_PATH='/some/local/folder/to/gitRepo'

# hostname of remote machine:
REMOTE_HOST='your.remote.host.com'

# where the localfile will sync to on REMOTE_HOST
# NOTE: format must match example
REMOTE_PATH='/path/of/remote/host/to/sync/gitRepo'

# Infinite loop to send data
while true; do

    # rsync command to synchronize files
    rsync -avzh $LOCAL_PATH/* $REMOTE_HOST:$REMOTE_PATH --delete
    
    # sleep for 1 second 
    sleep 1

done
